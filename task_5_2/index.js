const express = require('express');
const bodyParser = require('body-parser');

const router = express.Router();
const app = express();

var usersData = [];
var result;

var RPCmethods = {
    create: function(params) {
        if ((!params.name) || (!params.score)) {
            result = {
                error: "Обязательные параметры name и score!"
            };
            return result;
        }
        usersData.push(params);
        result = {
            id: usersData.length - 1,
            user: params
        };

        return result;
    },

    read: function(params) {
        var id = params.id;
        var user = usersData[id];
        if (user) {
            result = {
                "user": user
            }
        } else {
            result = {
                "error": "Not found user"
            }
        }

        return result;
    },

    edit: function(params) {
        var id = params.id;
        var user = usersData[id];
        if (user) {
            delete params.id;
            usersData[id] = Object.assign(user, params);
            result = {
                "id" : id,
                "user": usersData[id]
            };
        } else {
            result = {
                "error": "Not found user"
            }
        }

        return result;
    },

    del: function(params) {
        var id = params.id;
        var user = usersData[id];
        if (user) {
            delete usersData[id];
            result = usersData;
        } else {
            result = {
                "error": "Not found user"
            }
        }

        return result;
    }
};

app.use(bodyParser.json());

router.post("/users/", function(req, res) {
    if (RPCmethods[req.body.method]) {
        var methodResult = RPCmethods[req.body.method](req.body.params);
        res.json({
            "jsonrpc": 2.0,
            "result": methodResult,
            "id": req.body.id
        });
    } else {
        res.json(
            { "jsonrpc": 2.0,
              "error": {
                "code": "‐32601",
                "message": "Not found method"
               },
              "id": req.body.id
            }
        );
    }
});

app.use('/', router);

app.listen(3000);
