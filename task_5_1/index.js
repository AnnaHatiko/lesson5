const express = require('express');
const bodyParser = require('body-parser');

const router = express.Router();
const app = express();

var usersData = [];

app.use(bodyParser.json());

router.get("/users/", function(req, res) {
    res.json(usersData.filter(function(item) {
        return item;
    }));
});

router.get("/users/:id", function(req, res) {
    const { id } = req.params;
    const user = usersData[id];
    if (user) {
        res.json({
            user
        }) ;
    } else {
        res.status(404);
        res.json({});
    }
});

router.post("/users/", function(req, res) {
    if ((!req.body.name) || (!req.body.score)) {
        res.status(400);
        res.json( {
            error: 'Обязательные параметры name и score!'
        });
        return;
    }

    usersData.push(req.body);
    res.json({
        id: usersData.length - 1,
        user: req.body
    });
});

router.delete("/users/:id", function(req, res) {
    const { id } = req.params;
    const user = usersData[id];
     if (user) {
         usersData[id] = null;
     } else {
         res.status(404);
         res.json( {
             error: 'Такой user не найден'
         });
     }
});

router.put("/users/:id", function(req, res) {
    const { id } = req.params;
    const user = usersData[id];
    if (user) {
        usersData[id] = Object.assign(user, req.body);
        res.json({
            id,
            user: usersData[id]
        })
    } else {
        res.status(404);
        res.json({});
    }
});

app.use('/', router);

app.listen(3000);